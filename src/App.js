import React from 'react';
import {
  BrowserRouter as Router,
  Link,
  Route
} from "react-router-dom";
import logo from './logo.svg';
import './App.css';
import ErrorBoundary from './components/ErrorBoundary';
import PublicPage from './components/publicPage';
import PrivatePage from './components/privatePage';
import PrivateRoute from './components/privateRoute';
import LoginComponent from './components/login';
import { LoginContext } from './context/loginContext';


class App extends React.Component {
  constructor(props){
    super(props)
    this.state = {authContext : false}
  }
  updateAuthContext = value => {
    this.setState({authContext: value})
  }
  render(){
    return (
      <ErrorBoundary>
        <Router>
          <LoginContext.Provider value={[{isAuthenticated: this.state.authContext}, this.updateAuthContext]}>
        <div className="App">
          <h1>
            Helloo Welcome.
          </h1>
          <ul>
            <h3><Link to="/publicPage">View Public Page</Link></h3>
            <h3><Link to="/privatePage">View Weather Info</Link></h3>
          </ul>
            <Route path="/publicPage" component={PublicPage} />
            <PrivateRoute path="/privatePage" component={() => <PrivatePage />} />
            <Route path="/login" component={LoginComponent} />
        </div>
        </LoginContext.Provider>
        </Router>
      </ErrorBoundary>
    );
  }
}

export default App;
