import React from 'react';
import {
    Route,
    Redirect
} from 'react-router-dom'
import { LoginContext } from '../../context/loginContext';
class PrivateRoute extends React.Component{
    static contextType = LoginContext;
    render(){
        const {component: Component, ...rest} = this.props
        const [authContext] = this.context
        return (
            <Route {...rest} render={(props) => (
                authContext.isAuthenticated === true
                  ? <Component {...props} />
                  : <Redirect to={{
                      pathname: '/login',
                      state: { from: props.location }
                    }} />
              )} />
        )
    }
}

export default PrivateRoute