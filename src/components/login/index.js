import React from 'react'
import checkValidPassword from '../../utils/authRouter'
import { LoginContext } from '../../context/loginContext'
import {  useLocation, useHistory } from 'react-router-dom'


class LoginComponent extends React.Component{
    constructor(props){
        super(props)
        this.state = {password: ''}
    }
    static contextType = LoginContext;
    checkAuthentication = () => {
        const [,updateAuthContext] = this.context
        const isValid = checkValidPassword(this.state.password)
        if(isValid){
            updateAuthContext(true)
            let { from } = this.props.location.state || { from: { pathname: "/" } };
            this.props.history.replace(from.pathname);
        }else{
            alert('invalid password')
        }
    }
    render(){
        return (
            <>
            <h4>Login to access weather data</h4>
            <label>Enter password: </label>
            <input type="password" onChange={ (e) => this.setState({password: e.target.value}) } value={this.state.password} />
            <button onClick={this.checkAuthentication}>Submit</button>
            </>
        )
    }
}

export default LoginComponent