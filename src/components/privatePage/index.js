import React from 'react'
import Button from '../Button'
import List from '../List'
import { LoginContext } from '../../context/loginContext'

class PrivatePage extends React.Component{
    static contextType = LoginContext
    render(){
        const [authContext, updateAuthContext] = this.context
        return (
            <>
                {authContext.isAuthenticated ? <button onClick={() => updateAuthContext({isAuthenticated : false})}>Logout</button> : ''}
                <Button />
                <List />
            </>
        )
    }
}

export default PrivatePage